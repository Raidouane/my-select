/*
** manage_selection_enter_resize.c for manage_selection_enter_resize.C in /home/el-mou_r/rendu/PSU_2015_my_select/src
** 
** Made by Raidouane EL MOUKHTARI
** Login   <el-mou_r@epitech.net>
** 
** Started on  Fri Dec 11 19:21:56 2015 Raidouane EL MOUKHTARI
** Last update Sun Dec 13 22:08:56 2015 Raidouane EL MOUKHTARI
*/

# include <ncurses/curses.h>
#include <ncurses.h>
#include <string.h>
#include "../include/mylist.h"
#include "../include/my.h"
#include <stdlib.h>

t_list		*manage_selection(t_list *tmp, int ch, t_struct *se)
{
  t_list	*tmpa;

  if (ch == ' ')
    {
      tmpa = tmp;
      while (tmpa->pos != se->curs)
        tmpa = tmpa->next;
      if (tmpa->select == 0)
        tmpa->select = 1;
      else if (tmpa->select == 1)
        tmpa->select = 0;
      if (se->curs < se->save_last_pos)
        se->curs++;
      else
        se->curs = 1;
    }
  return (tmp);
}

t_list	*create_my_list_of_validations(t_list *tmp, t_struct *se)
{
  se->elem = malloc(sizeof(t_list));
  se->elem->data = tmp->data;
  se->elem->next = se->tmpa;
  if (se->tmpa != NULL)
    se->tmpa->prev = se->elem;
  se->tmpa = se->elem;
  if (se->tmpa->next == NULL)
    se->tmpb = se->tmpa;
}

void		display_my_list_validated(t_struct se)
{
  t_list	*tmp;

  se.tmpb->next = se.tmpa;
  se.tmpa->prev = se.tmpb;
  tmp = se.tmpa->next;
  while (se.tmpa != tmp)
    {
      se.tmpa = se.tmpa->prev;
      my_putstr(se.tmpa->data);
      my_putchar(' ');
    }
  se.tmpa = se.tmpa->prev;
  my_putstr(se.tmpa->data);
  my_putchar('\n');
}



void		manage_validation(t_list *list)
{
  t_list	*tmp;
  t_struct	se;
  int		selection;

  selection = 0;
  tmp = list;
  se.tmpa = NULL;
  while (tmp != list->prev)
    {
      if (tmp->select == 1)
        {
          create_my_list_of_validations(tmp, &se);
          selection = 1;
        }
      tmp = tmp->next;
    }
  if (tmp->select == 1)
    {
      create_my_list_of_validations(tmp, &se);
      selection = 1;
    }
  if (selection == 1)
    display_my_list_validated(se);
}

int	manage_enter(int ch, t_list *tmp, t_struct *se)
{
  se->i = 0;
  if (ch == '\n')
    {
      endwin();
      manage_validation(tmp);
      free_my_elems(tmp, se);
      exit(1);
    }
}

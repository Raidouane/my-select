/*
** my_popup.c for my_popup.c in /home/el-mou_r/rendu/PSU_2015_my_popup
** 
** Made by Raidouane EL MOUKHTARI
** Login   <el-mou_r@epitech.net>
** 
** Started on  Wed Dec  2 14:36:41 2015 Raidouane EL MOUKHTARI
** Last update Sun Dec 13 14:03:04 2015 Raidouane EL MOUKHTARI
*/

#include <ncurses/curses.h>
#include <ncurses.h>
#include <string.h>
#include "../include/mylist.h"
#include "../include/my.h"
#include <stdlib.h>

void		free_my_elems(t_list *tmp, t_struct *se)
{
  int		i;
  t_list	*tmpa;
  i = 0;
  while (i < se->save_last_pos)
    {
      tmpa = tmp->next;
      free(tmp);
      tmp = tmpa;
      i++;
    }
}

int	escape_gestion(int ch, t_list *tmp, t_struct *se)
{
  nodelay(stdscr, TRUE);
  if (getch() == ERR)
    {
      endwin();
      free_my_elems(tmp, se);
      exit (1);
    }
  nodelay(stdscr, FALSE);
}

int	init_my_variables(t_list *tmp, t_struct *se, int ch)
{
  se->col = 0;
  se->i = 0;
  se->lines = 0;
  se->enter = 0;
  se->curs = 1;
  se->save_last_pos = tmp->prev->pos;
  newterm(NULL, stderr, stdin);
  keypad(stdscr, TRUE);
  curs_set(0);
  start_color();
  init_pair(1, COLOR_RED, COLOR_BLACK);
  init_pair(2, COLOR_GREEN, COLOR_BLACK);
  wbkgd(stdscr, COLOR_PAIR(2));
  noecho();
  se->cont = 0;
  ESCDELAY = 10;
  set_escdelay(10);
  ch = ' ';
  return (ch);
}

int	check_resize_and_manage_display(t_struct *se, int my_len, t_list *tmp)
{
  if (se->col + tmp->len >= se->max_cols)
    {
      endwin();
      my_putstr("error output\n");
      exit (1);
    }
  if (se->lines == se->max_lines)
    {
      se->lines = 0;
      se->col = se->col + my_len + 1;
    }
}

void	display_my_elems_on_the_screen(t_struct *se, t_list **tmp, int ch)
{
  refresh();
  if (se->curs != (*tmp)->pos)
    attrset(A_NORMAL);
  if (se->curs == (*tmp)->pos)
    {
      attrset(COLOR_PAIR(1));
      attron(A_UNDERLINE);
      *tmp = manage_backspace(*tmp, ch, se);
    }
  if ((*tmp)->select == 1)
    attron(A_REVERSE);
  refresh();
  mvprintw(se->lines, se->col, "%s", (*tmp)->data);
  attron(COLOR_PAIR(2));
  mvprintw(se->max_lines - 1, se->max_cols - 18, "[%d]", se->save_last_pos);
  attroff(COLOR_PAIR(1));
  *tmp = (*tmp)->next;
  se->i++;
  se->lines++;
}

int   		display_on_the_terminal(t_list *tmp, int my_biggest_len)
{
  int		ch;
  t_struct	se;

  ch = init_my_variables(tmp, &se, ch);
  while (1)
    {
      refresh();
      getmaxyx(stdscr, se.max_lines, se.max_cols);
      while (se.i < se.save_last_pos)
	{
	  check_resize_and_manage_display(&se, my_biggest_len, tmp);
	  display_my_elems_on_the_screen(&se, &tmp, ch);
	}
      ch = getch();
      manage_selection(tmp, ch, &se);
      if (ch == 0x1B)
	escape_gestion(ch, tmp, &se);
      manage_keypad_arrows(ch, &se, tmp);
      manage_enter(ch, tmp, &se);
      se.col = 0;
      se.lines = 0;
      clear();
    }
  endwin();
}

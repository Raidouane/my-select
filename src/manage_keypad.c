/*
** manage_keypad.c for manage_keypad.c in /home/el-mou_r/rendu/PSU_2015_my_select/last/PSU_2015_my_select/src
** 
** Made by Raidouane EL MOUKHTARI
** Login   <el-mou_r@epitech.net>
** 
** Started on  Fri Dec 11 23:20:29 2015 Raidouane EL MOUKHTARI
** Last update Sat Dec 12 02:03:21 2015 Raidouane EL MOUKHTARI
*/

#include <ncurses/curses.h>
#include <string.h>
#include "../include/mylist.h"
#include "../include/my.h"

void	manage_arrows_rights_left(int ch, t_struct *se)
{
  if (ch == KEY_RIGHT)
    {
      se->curs = se->curs + se->max_lines;
      if (se->curs > se->save_last_pos)
        se->curs = 1;
    }
  if (ch == KEY_LEFT)
    {
      se->curs = se->curs - se->max_lines;
      if (se->curs < 1)
        se->curs = se->save_last_pos;
    }
}

int	manage_keypad_arrows(int ch, t_struct *se, t_list *tmp)
{
  if (ch == KEY_UP)
    {
      if (se->curs == 1)
        se->curs = se->save_last_pos + 1;;
      se->curs--;
    }
  manage_arrows_rights_left(ch, se);
  if (ch == KEY_DOWN)
    {
      if (se->curs == se->save_last_pos)
        se->curs = 0;
      se->curs++;
    }
}

/*
** manage_backspace.c for manage_backspace.c in /home/el-mou_r/rendu/PSU_2015_my_select/src
** 
** Made by Raidouane EL MOUKHTARI
** Login   <el-mou_r@epitech.net>
** 
** Started on  Thu Dec 10 01:59:29 2015 Raidouane EL MOUKHTARI
** Last update Sun Dec 13 01:05:54 2015 Raidouane EL MOUKHTARI
*/

# include <ncurses/curses.h>
#include <ncurses.h>
#include <string.h>
#include "../include/mylist.h"
#include "../include/my.h"
#include <stdlib.h>

t_list		*manage_before_last_elem_backspace(t_list *tmp, t_struct *se)
{
  t_list	*tmpa;
  t_list	*tmpb;

  tmpa = tmp->prev;
  tmpb = tmp->next;
  free(tmp);
  tmpa->next = tmpb;
  tmpb->prev = tmpa;
  tmp = tmpb;
  tmp->pos--;
  se->enter = 1;
  se->save_last_pos--;
  return (tmp);
}

t_list		*manage_basic_backspace(t_list *tmp, t_struct *se)
{
  t_list	*tmpa;
  t_list	*tmpb;

  tmpa = tmp->prev;
  tmpb = tmp->next;
  free(tmp);
  tmpa->next = tmpb;
  tmpb->prev = tmpa;
  tmp = tmpb;
  tmpa = tmp;
  while (tmpa->pos > 1)
    {
      tmpa->pos--;
      tmpa = tmpa->next;
    }
  se->save_last_pos--;
  return (tmp);
}

t_list		*manage_last_elem_backspace(t_list *tmp, t_struct *se)
{
  t_list	*tmpa;
  t_list	*tmpb;

  tmpa = tmp->prev;
  tmpb = tmp->next;
  free(tmp);
  tmpa->next = tmpb;
  tmpb->prev = tmpa;
  tmp = tmpa;
  se->curs--;
  se->save_last_pos--;
  se->cont = 1;
  return (tmp);
}

t_list	*manage_backspace(t_list *tmp, int ch, t_struct *se)
{
  if (ch == KEY_BACKSPACE || ch == KEY_DC)
    {
      se->cont = 0;
      if (se->save_last_pos == 1)
        {
          free(tmp);
          endwin();
          exit (1);
        }
      if (tmp->next->pos == 1)
        tmp = manage_last_elem_backspace(tmp, se);
      else if (tmp->next->next->pos != 1)
        tmp = manage_basic_backspace(tmp, se);
      else if (tmp->next->next->pos == 1)
        tmp = manage_before_last_elem_backspace(tmp, se);
    }
  return (tmp);
}

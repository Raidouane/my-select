/*
** create_my_list.c for create_my_list.c in /home/el-mou_r/rendu/PSU_2015_my_select/src
** 
** Made by Raidouane EL MOUKHTARI
** Login   <el-mou_r@epitech.net>
** 
** Started on  Thu Dec  3 13:09:35 2015 Raidouane EL MOUKHTARI
** Last update Sun Dec 13 13:57:40 2015 Raidouane EL MOUKHTARI
*/

#include <stdlib.h>
#include <stdio.h>
#include "../include/mylist.h"
#include "../include/my.h"

t_list	*create_list_last_step(t_list *tmp, t_list *la)
{
  tmp = tmp->prev;
  tmp->next = la;
  la->prev = tmp;
  return (la);
}

t_list		*create_my_list(int ac, char **av)
{
  t_list	*elem;
  t_list	*tmp;
  t_list	*la;
  int		i;

  la = NULL;
  i = 1;
  while (ac > 0)
    {
      if ((elem = malloc(sizeof(t_list))) == NULL)
	return (NULL);
      elem->data = av[ac];
      elem->pos = ac;
      elem->len = my_strlen(av[ac]);
      elem->select = 0;
      elem->supp = 0;
      elem->next = la;
      if (la != NULL)
	la->prev = elem;
      la = elem;
      ac--;
      i++;
      if (la->next == NULL)
	tmp = la;
    }
  return (create_list_last_step(tmp, la));
}

int		find_my_biggest_word(t_list *la)
{
  int		i;
  t_list	*tmp;

  tmp = la;
  i = 0;
  while (tmp != la->prev)
    {
      if (i < tmp->len)
	i = tmp->len;
      tmp = tmp->next;
    }
  if (i < tmp->len)
    i = tmp->len;
  return (i);
}

int		main(int ac, char **av)
{
  t_list	*la;
  t_list	*tmp;
  int		i;

  if (ac == 1)
    return (0);
  la = create_my_list(ac, av);
  i = find_my_biggest_word(la);
  display_on_the_terminal(la, i);
}

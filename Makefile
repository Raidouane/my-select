##
## Makefile for Makefile in /home/el-mou_r/rendu/CPE_2015_BSQ
## 
## Made by Raidouane EL MOUKHTARI
## Login   <el-mou_r@epitech.net>
## 
## Started on  Wed Dec  9 02:26:38 2015 Raidouane EL MOUKHTARI
## Last update Sat Dec 12 01:24:22 2015 Raidouane EL MOUKHTARI
##

CC	= gcc -g

RM	= rm

NAME	= my_select

FLAG	= -I./include -Wall -W -lncurses -Werror

SRC	=src/my_popup.c			\
	src/my_putchar.c		\
	src/my_putstr.c			\
	src/manage_selection_enter.c	\
	src/my_strlen.c			\
	src/manage_backspace.c		\
	src/create_my_list.c		\
	src/manage_keypad.c

OBJ	= $(SRC:.c=.o)

$(NAME): $(OBJ)
	$(CC) $(OBJ) $(FLAG) -o $(NAME)

all:	$(NAME)

clean:
	$(RM) -f $(OBJ)

fclean:	clean
	$(RM) -f $(NAME)

re:	fclean all

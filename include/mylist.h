/*
** mylist.h for mylist.h in /home/el-mou_r/rendu/PSU_2015_my_select/include
** 
** Made by Raidouane EL MOUKHTARI
** Login   <el-mou_r@epitech.net>
** 
** Started on  Thu Dec  3 13:23:26 2015 Raidouane EL MOUKHTARI
** Last update Sun Dec 13 13:54:34 2015 Raidouane EL MOUKHTARI
*/

#ifndef MY_LIST_H_
# define MY_LIST_H_

typedef struct	s_list
{
  char		*data;
  int		pos;
  int		supp;
  int		len;
  int		select;
  struct s_list	*prev;
  struct s_list	*next;
}		t_list;

typedef struct	s_struct
{
  int		save_last_pos;
  int		enter;
  int		col;
  int		lines;
  int		i;
  int		curs;
  int		stop;
  int		max_cols;
  int		max_lines;
  int		cont;
  t_list	*elem;
  t_list	*tmp;
  t_list	*tmpa;
  t_list	*tmpb;
}		t_struct;

#endif

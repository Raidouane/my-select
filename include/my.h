/*
** my.h for my.h in /home/el-mou_r/rendu/PSU_2015_my_select/include
** 
** Made by Raidouane EL MOUKHTARI
** Login   <el-mou_r@epitech.net>
** 
** Started on  Fri Dec 11 20:32:01 2015 Raidouane EL MOUKHTARI
** Last update Sat Dec 12 00:56:37 2015 Raidouane EL MOUKHTARI
*/

# ifndef _MY_H_
#define _MY_H_
#include "mylist.h"

int	find_my_biggest_word(t_list *la);
t_list	*create_my_list(int ac, char **av);
t_list	*create_my_list_next(t_list *tmp, t_list *la);
int	init_my_variables(t_list *tmp, t_struct *se, int ch);
int	check_resize_and_manage_display(t_struct *se, int my_len, t_list *tmp);
void	display_my_elems_on_the_screen(t_struct *se, t_list **tmp, int ch);
t_list	*manage_basic_backspace(t_list *tmp, t_struct *se);
t_list	*manage_before_last_elem_backspace(t_list *tmp, t_struct *se);
t_list	*manage_last_elem_backspace(t_list *tmp, t_struct *se);
t_list	*manage_backspace(t_list *tmp, int ch, t_struct *se);
t_list	*manage_selection(t_list *tmp, int ch, t_struct *se);
int	escape_gestion(int ch, t_list *tmp, t_struct *se);
void	free_my_elems(t_list *tmp, t_struct *se);
int	manage_enter(int ch, t_list *tmp, t_struct *se);
void	manage_arrows_rights_left(int ch, t_struct *se);
int	manage_keypad_arrows(int ch, t_struct *se, t_list *tmp);
t_list	*create_my_list_of_validations(t_list *tmp, t_struct *se);
void	display_my_list_validated(t_struct se);
void	manage_validation(t_list *list);
void	my_putchar(char c);
void	my_putstr(char *str);
int	my_strlen(char *str);

#endif /* _MY_H_ */
